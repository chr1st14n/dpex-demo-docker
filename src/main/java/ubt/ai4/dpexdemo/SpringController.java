package ubt.ai4.dpexdemo;

import dpex.action.Instantiation;
import dpex.core.DPEXService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("collab")
@RequiredArgsConstructor
public class SpringController {

    private final DPEXService dpexService;
    @PostMapping("dply")
    public ResponseEntity<?> instantiate(@RequestBody Instantiation instantiation) {

        dpexService.handleInstantiation(instantiation);

        return ResponseEntity.ok().build();

    }
}
