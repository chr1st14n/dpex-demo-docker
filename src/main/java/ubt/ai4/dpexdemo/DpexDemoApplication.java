package ubt.ai4.dpexdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories({"dpex", "ubt.ai4.dpexdemo"})
@EntityScan(basePackages ={"dpex", "ubt.ai4.dpexdemo"})
@ComponentScan(basePackages={"dpex", "dpex.db", "dpex.impl.bpm.camunda",  "ubt.ai4.dpexdemo"})
@SpringBootApplication
public class DpexDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DpexDemoApplication.class, args);
	}

}
