package ubt.ai4.dpexdemo.camunda.org;

import dpex.impl.bpm.camunda.CamundaModelFactory;
import lombok.Data;
import ubt.ai4.dpexdemo.org.wrp.model.OrganizationalModel;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class CamundaMPModelFactory extends CamundaModelFactory {

    @Lob
    private List<Object> workflowResourcePattern;

    @Lob
    String organizationalModel;
    @ElementCollection
    List<String> workflowResourcePatterns;


    public CamundaMPModel create() {
        CamundaMPModel camundaMPModel = new CamundaMPModel();
        camundaMPModel.setBpmnModel(super.getBpmnModel());
        camundaMPModel.setParticipants(super.getParticipants());
        camundaMPModel.setModelReference(super.getName());
        camundaMPModel.setOrganizationalModel(this.getOrganizationalModel());
        camundaMPModel.setWorkflowResourcePatterns(this.getWorkflowResourcePatterns());

        return camundaMPModel;
    }


}
