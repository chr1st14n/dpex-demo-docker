package ubt.ai4.dpexdemo.camunda.org;

import dpex.impl.bpm.camunda.CamundaModel;
import lombok.*;
import ubt.ai4.dpexdemo.org.wrp.model.OrganizationalModel;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import java.util.List;

@Entity
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class CamundaMPModel extends CamundaModel {

    @ElementCollection
    List<String> workflowResourcePatterns;

    @Lob
    String organizationalModel;

    public void foo() {

    }


}
