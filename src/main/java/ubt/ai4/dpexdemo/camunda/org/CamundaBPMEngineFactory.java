package ubt.ai4.dpexdemo.camunda.org;

import dpex.bpm.execution.BPMEngine;
import dpex.bpm.execution.BPMEngineFactory;
import dpex.bpm.model.ProcessModel;
import dpex.impl.bpm.camunda.CamundaExecutionEngineFactory;
import dpex.impl.bpm.camunda.CamundaModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CamundaBPMEngineFactory extends CamundaExecutionEngineFactory {

    private String camundaUrl;

    @Override
    public CamundaBPMEngine create(ProcessModel processModel) {
        CamundaBPMEngine cem = new CamundaBPMEngine();
        cem.deploy(((CamundaModel) processModel).getBpmnModel());

        cem.setCamundaUrl(this.camundaUrl);
        cem.setName(super.getName());
        return cem;
    }

}
