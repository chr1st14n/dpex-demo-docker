package ubt.ai4.dpexdemo.camunda.org;

import dpex.core.User;
import dpex.util.DpexHttpResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.RequestEntity;

import javax.persistence.Entity;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class CamundaManager {
    public void createCamundaUsers(CamundaBPMEngine engine, Set<User> user){
        HttpClient httpClient = HttpClient.newHttpClient();
        String connection = engine.getCamundaUrl();
        String authValue = new String(Base64.encodeBase64("demo:demo".getBytes(StandardCharsets.UTF_8)));
        for(User u : user){
            String name = u.getBpmId();
            sendUserCreationRequest(name, connection, httpClient, authValue);
            sendUserGroupAddingRequest(name, connection, httpClient, authValue);
        }
    }

    public void sendUserCreationRequest(String name, String camundaConnection, HttpClient httpClient, String authValue){
        //Here the password and the username for the admin user is hardcoded. Obviously this should be changed when starting to use
        //this in a production environment
        String url = camundaConnection + "user/create";
        String body = "{" +
                "    \"profile\": \n" +
                "\n" +
                "{\n" +
                "\n" +
                "    \"id\": \""+name+"\",\n" +
                "    \"firstName\": \""+name+"\",\n" +
                "    \"lastName\": \""+name+"\",\n" +
                "    \"email\": \"anEmailAddress\"\n" +
                "\n" +
                "},\n" +
                "\"credentials\": \n" +
                "{\n" +
                "\n" +
                "    \"password\": \""+name+"\"\n" +
                "\n" +
                "}" +
                "}";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Authorization", "Basic " + authValue)
                .build();

        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void sendUserGroupAddingRequest(String userId, String connection, HttpClient httpClient, String authValue){
        //Here the password and the username for the admin user is hardcoded. Obviously this should be changed when starting to use
        //this in a production environment#
        // http://{host}:{port}/{contextPath}/group/{id}/members/{userId}
        String url = connection + "group/camunda-admin/members/" + userId  ;
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString(""))
                .setHeader("Authorization", "Basic " + authValue)
                .build();

        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
