package ubt.ai4.dpexdemo.org.wrp.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Position {

    @Id
    private String name;

    @ManyToOne
    Position hasDirectReport;

    @ManyToOne
    Position canDelegateWorkItemsTo;

    @ManyToOne
    OrganizationalGrouping isMemberOf;

    @ManyToOne
    Privilege possesses;

    @ManyToOne
    Role participatesIn;
}
