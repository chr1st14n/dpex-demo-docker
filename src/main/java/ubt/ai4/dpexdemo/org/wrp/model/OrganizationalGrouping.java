package ubt.ai4.dpexdemo.org.wrp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationalGrouping {

    @Id
    private String name;

    @ManyToOne
    OrganizationalGrouping isPartOf;

    @ManyToOne
    OrganizationalGroupingType isOf;
}
