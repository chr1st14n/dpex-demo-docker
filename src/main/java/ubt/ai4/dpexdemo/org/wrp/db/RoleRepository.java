package ubt.ai4.dpexdemo.org.wrp.db;

import org.springframework.data.jpa.repository.JpaRepository;
import ubt.ai4.dpexdemo.org.wrp.model.Role;

@org.springframework.stereotype.Repository
public interface RoleRepository extends JpaRepository<Role, String> {
}
