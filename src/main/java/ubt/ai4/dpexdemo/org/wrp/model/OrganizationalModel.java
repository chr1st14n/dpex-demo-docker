package ubt.ai4.dpexdemo.org.wrp.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
public class OrganizationalModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany
    Collection<HumanResource> humanResources;


}
