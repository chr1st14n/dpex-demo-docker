package ubt.ai4.dpexdemo.org.wrp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationalGroupingType {

    @Id
    String name;

}
