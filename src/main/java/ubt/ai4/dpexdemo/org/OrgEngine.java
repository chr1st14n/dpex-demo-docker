package ubt.ai4.dpexdemo.org;

import dpex.action.TaskAction;
import dpex.bpm.execution.EventLog;
import dpex.core.Event;
import dpex.core.User;
import dpex.impl.bpm.model.bpmn.Activity;
import dpex.impl.bpm.model.bpmn.BPMNModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@Service
public class OrgEngine {

    private static final Logger logger = LogManager.getLogger(OrgEngine.class);

    private static final BiFunction<String, ProcessData, Boolean> roleFunction = new BiFunction<String, ProcessData, Boolean>() {
        @Override
        public Boolean apply(String role, ProcessData input) {

            boolean hasRole = false;

            for(String line : input.getOrganizationalModel().split(OrgUtils.lineSplitter)) {
                if(line.startsWith(input.getTaskAction().getUser().getBpmId())) {
                    List<String> roles = Arrays.asList(line.split(",")[OrgUtils.rolesIndex].split("\\|"));
                    hasRole = roles.contains(role);
                }
            }

            return hasRole;
        }
    };

    private static final BiFunction<String, ProcessData, Boolean> departmentFunction = new BiFunction<String, ProcessData, Boolean>() {
        @Override
        public Boolean apply(String department, ProcessData input) {

            boolean inDepartment = false;

            for(String line : input.getOrganizationalModel().split(OrgUtils.lineSplitter)) {
                if(line.startsWith(input.getTaskAction().getUser().getBpmId())) {
                    List<String> departments = Arrays.asList(line.split(",")[OrgUtils.departmentsIndex].split("\\|"));
                    inDepartment = departments.contains(department);
                }
            }

            return inDepartment;

        }
    };

    private static final BiFunction<String, ProcessData, Boolean> historyFunction = new BiFunction<String, ProcessData, Boolean>() {
        @Override
        public Boolean apply(String historicalConstraint, ProcessData input) {

            boolean isConform = false;

            String currentResource = input.getTaskAction().getUser().getBpmId();

            String historicalTask = historicalConstraint.split(",")[0];
            String organizationalEntity = historicalConstraint.split(",")[1];

            EventLog log = input.getEventLog();
            Event historicalEvent = log.getEvents().stream().filter(e -> e.getActivity().equals(historicalTask)).findFirst().orElseThrow(() -> new RuntimeException());

            switch(organizationalEntity) {
                case "resource":
                    isConform = historicalEvent.getResource().equals(input.getTaskAction().getUser().getBpmId());
                    break;
                case "role":
                    List<String> currentRoles = Arrays.stream(input.getOrganizationalModel().split(OrgUtils.lineSplitter)).toList().stream()
                            .filter(line -> line.startsWith(currentResource))
                            .map(line -> line.split(",")[2])
                            .collect(Collectors.toList());

                    List<String> historicalRoles = Arrays.stream(input.getOrganizationalModel().split(OrgUtils.lineSplitter)).toList().stream()
                            .filter(line -> line.startsWith(historicalEvent.getResource()))
                            .map(line -> line.split(",")[2])
                            .collect(Collectors.toList());

                    isConform = !Collections.disjoint(currentRoles, historicalRoles);

                    break;
                case "department":
                    List<String> currentDepartments = Arrays.stream(input.getOrganizationalModel().split(OrgUtils.lineSplitter)).toList().stream()
                            .filter(line -> line.startsWith(currentResource))
                            .map(line -> line.split(",")[3])
                            .collect(Collectors.toList());

                    List<String> historicalDepartments = Arrays.stream(input.getOrganizationalModel().split(OrgUtils.lineSplitter)).toList().stream()
                            .filter(line -> line.startsWith(historicalEvent.getResource()))
                            .map(line -> line.split(",")[3])
                            .collect(Collectors.toList());

                    isConform = !Collections.disjoint(currentDepartments, historicalDepartments);
                    break;
            }

            return isConform;

        }
    };

    private static final BiFunction<String, ProcessData, Boolean> directAllocationFunction = new BiFunction<String, ProcessData, Boolean>() {
        boolean isAllocated = false;
        @Override
        public Boolean apply(String user, ProcessData input) {

            for(String line : input.getOrganizationalModel().split(OrgUtils.lineSplitter)) {
                if(line.startsWith(input.getTaskAction().getUser().getBpmId())) {
                    isAllocated = true;
                }
            }


            return isAllocated;
        }

    };

    private static final BiFunction<String, ProcessData, Boolean> bindingOfDutiesFunction = new BiFunction<String, ProcessData, Boolean>() {
        @Override
        public Boolean apply(String task, ProcessData input) {

            //find corresponding task from event log
            //get user
            Optional<Event> event = input.getEventLog().getEvents().stream().filter(e -> e.getActivity().equals(task)).findFirst();
            if(event.isPresent()) {
                // check if the current resource equals the resource that has executed the bindingOfDuty activity
                return input.getTaskAction().getUser().getBpmId().equals(event.get().getResource());
            }
            else {
                // the activity was not executed yet, so the restriction is not active yet
                return true;
            }
        }
    };


    private static Map<String, BiFunction<String, ProcessData, Boolean>> functionMapping;

    static {
        functionMapping = new HashMap<>();
        functionMapping.put("role", roleFunction);
        functionMapping.put("department", departmentFunction);
        functionMapping.put("history", historyFunction);
        functionMapping.put("direct_allocation", directAllocationFunction);
        functionMapping.put("bod", bindingOfDutiesFunction);
    }
    public boolean evaluate(BPMNModel bpmnModel, String organizationalModel, EventLog log, TaskAction taskAction) {


        Activity activity = bpmnModel.getActivities().stream().filter(a -> a.getName().equals(taskAction.getTask().getActivity())).findFirst().get();

        // no restrictions
        if(activity.getAnnotation() == null){
            logger.info("No annotation found ==> No organizational constraints apply.");
            return true;
        }

        if(taskAction.getUser() == null) {
            logger.warn("No UserReference given ==> Not model conform.");
            return false;
        }

        if(taskAction.getUser().getBpmId() == null) {
            logger.debug("UserReference not set at given User. Fetch from OrgModel by PublicKey.");
            User user = taskAction.getUser();
            String userReference = OrgUtils.getBpmIdFromSciId(organizationalModel, taskAction.getUser().getSciId());
            user.setBpmId(userReference);
            taskAction.setUser(user);
        }

        String annotation = activity.getAnnotation();
        //not using line splitter here because this string comes from camunda modeler, not from the frontend
        String[] lines = annotation.split("\n");
        for(String line : lines) {
	        String pattern = line.substring(0, line.indexOf("("));
	        BiFunction<String, ProcessData, Boolean> function = functionMapping.get(pattern);
	        boolean isConform = function.apply(
	                line.substring(line.indexOf("(")+1, line.indexOf(")")),
	                new ProcessData(organizationalModel, log, taskAction));
	        if(!isConform)
	        	return false;
        }
        return true;
    }

}

@Data
@AllArgsConstructor
class ProcessData {
    private String organizationalModel;
    private EventLog eventLog;
    private TaskAction taskAction;

}
