package ubt.ai4.dpexdemo.org;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ParticipantDTO{
	private String bpmId;
	private String communicationId;
	private String roles;
	private String departments;
}
