package ubt.ai4.dpexdemo;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import dpex.core.DPEXService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import ubt.ai4.dpexdemo.api.EventListener;

@Component
@RequiredArgsConstructor
public class Startup implements ApplicationListener<ApplicationReadyEvent> {
	
    private final EventListener eventListener;

    @SneakyThrows
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        DPEXService.eventBus.register(eventListener);
    }

}
