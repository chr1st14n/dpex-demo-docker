package ubt.ai4.dpexdemo.api;

import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.core.Alliance;
import dpex.core.User;
import dpex.db.AllianceRepository;
import dpex.error.TaskNotFoundException;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.Subscribe;

import dpex.action.TaskAction;
import dpex.communication.IncomingMessageEvent;
import dpex.events.InstantiationEvent;
import ubt.ai4.dpexdemo.camunda.org.CamundaBPMEngine;
import ubt.ai4.dpexdemo.camunda.org.CamundaManager;
import ubt.ai4.dpexdemo.utils.ScriptExecutor;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class EventListener {
	
    private final SimpMessagingTemplate simpMessagingTemplate;
	private final AllianceRepository allianceRepository;

	/**
	 * This event gets called after an instantiation. Creating camunda users matching the given organizationalModel. After the user creation all automated tasks
	 * get executed
	 * @param event
	 * @throws TaskNotFoundException
	 */
	@Subscribe
	public void instantiationEvent(InstantiationEvent event) throws TaskNotFoundException {
		simpMessagingTemplate.convertAndSend("/topic/instantiation/" + event.getGar(), event.getInstance());

		//Create camunda users
		//This code should obviously not be executed when the client is not using camunda
		CamundaManager cm = new CamundaManager();
		Alliance alliance = null;
		List<Alliance> alliances = allianceRepository.findAll();
		for(Alliance a: alliances){
			alliance = a;
		}
		CamundaBPMEngine engine = (CamundaBPMEngine) alliance.getEngine();
		Set<User> users = alliance.getModel().getParticipants();
		cm.createCamundaUsers(engine, users);

		//Manage scripts
		ScriptExecutor executor = new ScriptExecutor(allianceRepository);
		executor.manageScripts(event);

		//...
	}
	
	@Subscribe
	public void taskActionEvent(TaskAction taskAction) {
    	simpMessagingTemplate.convertAndSend("/topic/taskAction/" + taskAction.getInstance().getGlobalInstanceReference(), taskAction.getTask().getActivity());
	}

	@Subscribe
	public void incomingMessageEvent(IncomingMessageEvent incomingMessageEvent) {
        simpMessagingTemplate.convertAndSendToUser(incomingMessageEvent.getAccessToken(), "/queue", incomingMessageEvent.getIncomingMessageDTO());
	}

	/**
	 * This event gets called to check for automated tasks after a successful complete or claim (returning immediately in case of a CLAIM)
	 * @param taskAction
	 * @throws TaskNotFoundException
	 */
	@Subscribe
	public void checkForScripting(TaskAction taskAction) throws TaskNotFoundException {
		if(taskAction.getTaskLifeCycleStage() == TaskLifeCycleStage.CLAIM)
				return;
		ScriptExecutor executor = new ScriptExecutor(allianceRepository);
		executor.manageScripts(taskAction);
	}
}
