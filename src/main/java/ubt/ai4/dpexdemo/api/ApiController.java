package ubt.ai4.dpexdemo.api;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonProperty;

import dpex.action.Instantiation;
import dpex.bpm.execution.BPMEngineFactory;
import dpex.bpm.execution.BPMEngineType;
import dpex.bpm.execution.EventLog;
import dpex.bpm.execution.Instance;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.bpm.model.ProcessModelFactory;
import dpex.collaboration.CollaborationFactory;
import dpex.collaboration.CollaborationType;
import dpex.communication.CommunicationFactory;
import dpex.communication.CommunicationModuleType;
import dpex.core.Alliance;
import dpex.core.DPEXService;
import dpex.core.DPEXUtils;
import dpex.core.User;
import dpex.db.AllianceRepository;
import dpex.db.BPMEngineFactoryRepository;
import dpex.db.CollaborationFactoryRepository;
import dpex.db.CommunicationFactoryRepository;
import dpex.db.ProcessModelFactoryRepository;
import dpex.db.UserRepository;
import dpex.impl.bpm.model.bpmn.BPMNModel;
import dpex.impl.collaboration.ethereum.raw.EthereumRawConfig;
import dpex.impl.collaboration.ethereum.raw.EthereumRawFactory;
import dpex.impl.collaboration.hyperledger.HyperledgerFactory;
import dpex.impl.communication.DiscordAdapterFactory;
import dpex.impl.communication.MatrixAdapterFactory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import ubt.ai4.dpexdemo.camunda.org.CamundaBPMEngineFactory;
import ubt.ai4.dpexdemo.camunda.org.CamundaMPModel;
import ubt.ai4.dpexdemo.camunda.org.CamundaMPModelFactory;
import ubt.ai4.dpexdemo.malicious.MaliciousEngineFactory;
import ubt.ai4.dpexdemo.org.OrgUtils;
import ubt.ai4.dpexdemo.org.ParticipantDTO;
import ubt.ai4.dpexdemo.utils.ScriptManager;


@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ApiController {
    private final ProcessModelFactoryRepository baseModelFactoryRepository;
    private final AllianceRepository allianceRepository;
    private final UserRepository userRepository;
    private final DPEXUtils dpexUtils;
    private final DPEXService dpexService;
    private final BPMEngineFactoryRepository bpmEngineFactoryRepository;
    private final CollaborationFactoryRepository collaborationFactoryRepository;
    private final CommunicationFactoryRepository communicationFactoryRepository;
    
    @RequestMapping(value = "ethereum/deploySCI", method = RequestMethod.POST)
    public ResponseEntity<?> deploySci(@RequestBody DeploySciDTO deploySciDTO) {

        String gar = dpexUtils.deploySci(deploySciDTO.getConnection(), deploySciDTO.getSciId(), deploySciDTO.getApiKey());
        return ResponseEntity.ok().body(gar);

    }

    @RequestMapping(value = "model", method = RequestMethod.POST, consumes = {   "multipart/form-data" })
    public ResponseEntity<?> createProcessModel(ProcessModelDTO processModelDTO) {

        MultipartFile processModel = processModelDTO.getProcessModel();

        CamundaMPModelFactory modelFactory = new CamundaMPModelFactory();

        try {
            modelFactory.setBpmnModel(processModel.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        BPMNModel parsedBPMN = BPMNModel.fromString(new String(modelFactory.getBpmnModel()));

        modelFactory.setName(parsedBPMN.getProcessId());
        modelFactory.setOrganizationalModel(processModelDTO.getOrganizationalModel());

        // Set participants
        Set<User> users = fetchUsersFromOrganizationaModel(processModelDTO.getOrganizationalModel());

        /* avoid having a communication ID twice in the repository such that the request for retrieving the homeserver url
    	 * in the communication login function will always have a unique result
    	 * however, having empty string multiple times as communication ID is not problematic
    	 */
        users = users.stream().map(user -> {
        	if(user.getCommunicationId().equals(""))
        		return userRepository.save(user);
        	Optional<User> optionalUser = userRepository.findByCommunicationId(user.getCommunicationId());
        	if(optionalUser.isEmpty())
        		return userRepository.save(user);
        	else
        		return optionalUser.get();
        }).collect(Collectors.toSet());

		/**
		 * This user will be set as the executing one when executing automated tasks
		 * He gets added as participant to make sure he also gets created as Camunda user
		 */
		User localUser = new User();
		localUser.setBpmId("localUserForScriptExecution");
		userRepository.save(localUser);

        modelFactory.setParticipants(users);

        modelFactory = baseModelFactoryRepository.save(modelFactory);

        return ResponseEntity.ok().build();
    }

    private Set<User> fetchUsersFromOrganizationaModel(String organizationalModel) {
        String[] allLines = organizationalModel.split(OrgUtils.lineSplitter);
        //omitting the first line since it's the column names
        String[] lines = Arrays.copyOfRange(allLines, 1, allLines.length);

        return Arrays.stream(lines).map(line -> {
            String[] attributes = line.split(",");
            return User.builder()
                    .bpmId(attributes[OrgUtils.bpmIdIndex])
                    .sciId(attributes[OrgUtils.sciIdIndex])
                    .communicationId(attributes[OrgUtils.communicationIdIndex])
                    .homeServerUrl(attributes[OrgUtils.homeserverUrlIndex])
                    .build();
        }).collect(Collectors.toSet());
    }

    @RequestMapping(value = "alliance", method = RequestMethod.POST)
    public ResponseEntity<?> createAlliance(@ModelAttribute AllianceDTO allianceDTO) {

		/*

        HyperledgerConfig config = HyperledgerConfig.builder().build();
        config.setHyperledgerPath("\\\\wsl.localhost\\Ubuntu-22.04\\home\\tobias\\hyperledger\\fabric-samples\\");
        config.setHyperledgerChannel(System.getenv().getOrDefault("CHANNEL_NAME", "mychannel"));
        config.setHyperledgerChaincode(System.getenv().getOrDefault("CHAINCODE_NAME", "HyperledgerChaincode"));
        config.setMSP_ID(System.getenv().getOrDefault("MSP_ID", "Org1MSP"));
        config.setCryptoPath(Paths.get(config.getHyperledgerPath() + "/test-network/organizations/peerOrganizations/org1.example.com"));
        config.setCertificatePath(config.getCryptoPath().resolve(Paths.get("users/User1@org1.example.com/msp/signcerts/cert.pem")));
        config.setKeyDirectoryPath(config.getCryptoPath().resolve(Paths.get("users/User1@org1.example.com/msp/keystore")));
        config.setTLSCertificatePath(config.getCryptoPath().resolve(Paths.get("peers/peer0.org1.example.com/tls/ca.crt")));
        config.setOverrideAuth("peer0.org1.example.com");
        config.setGAR(allianceDTO.getGAR());

		*/

		EthereumRawConfig config = EthereumRawConfig.builder().sciContractAddress(allianceDTO.getGAR()).build();

        Alliance alliance = dpexUtils.buildAlliance(
                allianceDTO.getName(),
                allianceDTO.getProcessModel(),
                allianceDTO.getBpmEngine(),
                allianceDTO.getCollaboration(),
                allianceDTO.getCommunication(),
                allianceDTO.getGAR(),
                config
        );

        alliance = allianceRepository.save(alliance);
        
        return ResponseEntity.ok().body(alliance);

    }
    
    @DeleteMapping("alliance")
    public ResponseEntity<?> deleteAlliance(@RequestParam String name){
    	allianceRepository.deleteById(name);
    	return ResponseEntity.ok(null);
    }

    @PostMapping("instantiate")
    public ResponseEntity<?> instantiate(@RequestBody InstantiateDTO instantiateDTO) {

        Alliance alliance = allianceRepository.findByGlobalAllianceReference(instantiateDTO.getGar()).orElseThrow(() -> new RuntimeException());
        User user = OrgUtils.getUserFromBpmId(alliance.getModel(), instantiateDTO.getBpmId());
        Instantiation instantiation = dpexUtils.buildInstantiation(instantiateDTO.getGar(), alliance.getModel().getModelReference(), user, instantiateDTO.getVariables() != null ? instantiateDTO.getVariables() : "", instantiateDTO.getName());

        dpexService.sendInstantiation(alliance, instantiation);

        return ResponseEntity.ok().build();

    }

    @GetMapping("instances")
    public ResponseEntity<?> getInstances(@RequestParam String gar) {
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow(() -> new RuntimeException());
       // ((EthereumRawNetwork) alliance.getCollaboration().getNetwork()).getInstances();
        return ResponseEntity.ok().body(alliance.getEngine().getInstances());
    }
    
    @GetMapping("instance")
    public ResponseEntity<?> getInstance(@RequestParam String gir){
    	Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow();
    	Instance instance = alliance.getEngine().getInstances().stream().filter(instance_ -> instance_.getGlobalInstanceReference().equals(gir)).findFirst().orElseThrow();
    	return ResponseEntity.ok(instance);
    }
    
    @GetMapping("alliances")
    public ResponseEntity<?> getAlliances(@RequestParam String bpmId){
    	List<Alliance> allAlliances = allianceRepository.findAll();
    	
    	//Filter only the alliances in which the given user is member of
    	List<Alliance> alliances = new ArrayList<Alliance>();
    	for(Alliance alliance : allAlliances)
    		for(User participant : alliance.getModel().getParticipants())
    			if(participant.getBpmId().equals(bpmId)) {
    				alliances.add(alliance);
    				break;
    			}
    	
    	
    	GetAllianceDTO[] allianceDTOs = new GetAllianceDTO[alliances.size()];
    	int i = 0;
    	for(Alliance alliance : alliances) {
    		String name = alliance.getName();
    		String gar = alliance.getGlobalAllianceReference();
    		String processModel = alliance.getModel().getModelReference();
    		
    		CamundaMPModel camundaMPModel = (CamundaMPModel)alliance.getModel();
        	String organizationalModel = camundaMPModel.getOrganizationalModel();
    		ParticipantDTO[] participants = OrgUtils.getAllParticipantsWithRolesAndDepartments(organizationalModel);
    		
    		Set<Instance> instances = alliance.getEngine().getInstances();
    		
    		String communicationModule = null;
    		if(alliance.getCommunication() != null)
    			communicationModule = alliance.getCommunication().getName();
    		String collaboration = alliance.getCollaboration().getName();
    		String bpmEngine = alliance.getEngine().getName();
    		allianceDTOs[i++] = new GetAllianceDTO(name, gar, processModel, participants, instances.toArray(Instance[]::new), communicationModule, collaboration, bpmEngine);
    	}
    	return ResponseEntity.ok(allianceDTOs);
    }
    
    @GetMapping("myCommunicationModules")
    public ResponseEntity<?> getMyCommunicationModules(@RequestParam String bpmId){
    	Set<String> communicationModules = new HashSet<String>();
    	List<Alliance> alliances = allianceRepository.findAll();
    	for(Alliance alliance : alliances) {
    		Set<User> allUsersOfAlliance = alliance.getModel().getParticipants();
    		for(User userOfAlliance : allUsersOfAlliance) {
    			if(userOfAlliance.getBpmId().equals(bpmId)) {
    				if(alliance.getCommunication() != null) {
    					String communicationModule = alliance.getCommunication().getName();
    					communicationModules.add(communicationModule);
    				}
    	    		break;
    			}
    		}
    	}
    	return ResponseEntity.ok(communicationModules.toArray());
    }
    
    @GetMapping("collaborationTypes")
    public ResponseEntity<?> getCollaborationTypes(){
    	EnumValueDTO[] types = Arrays.stream(CollaborationType.values()).map(type -> new EnumValueDTO(type.name(), type.label)).toArray(EnumValueDTO[]::new);
    	return ResponseEntity.ok(types);
    }
    
    @GetMapping("bpmEngineTypes")
    public ResponseEntity<?> getBpmEngineTypes(){
    	EnumValueDTO[] types = Arrays.stream(BPMEngineType.values()).map(type -> new EnumValueDTO(type.name(), type.label)).toArray(EnumValueDTO[]::new);
    	return ResponseEntity.ok(types);
    }
    
    @GetMapping("communicationModuleTypes")
    public ResponseEntity<?> getCommunicationModuleTypes(){
    	EnumValueDTO[] types = Arrays.stream(CommunicationModuleType.values()).map(type -> new EnumValueDTO(type.name(), type.label)).toArray(EnumValueDTO[]::new);
    	return ResponseEntity.ok(types);
    }
    
    @PostMapping("collaboration")
    public ResponseEntity<?> createCollaboration(@RequestBody CollaborationConnectorDTO connectorDTO){
    	if(connectorDTO.getConnection() == null || connectorDTO.getConnection().equals(""))
    		throw new RuntimeException("A collaboration always requires a connection");
    	CollaborationType type = CollaborationType.valueOf(connectorDTO.getType());
    	switch(type){
    		case ETHEREUM_RAW:
    			EthereumRawFactory ethraw = new EthereumRawFactory();
    	        ethraw.setConnection(connectorDTO.getConnection());
    	        ethraw.setName(connectorDTO.getName());
    	        ethraw.setAuthHeader(connectorDTO.getApiKey());
    	        ethraw = collaborationFactoryRepository.save(ethraw);
    			break;
    		case HYPERLEDGER:
    			HyperledgerFactory hyperledgerFactory = new HyperledgerFactory();
    	        hyperledgerFactory.setConnection(connectorDTO.getConnection());
    	        hyperledgerFactory.setName(connectorDTO.getName());
    	        hyperledgerFactory = collaborationFactoryRepository.save(hyperledgerFactory);
    			break;
    		default:
    			throw new RuntimeException("Unknown Collaboration Type");
    	}
    	return ResponseEntity.ok(null);
    }
    
    @PostMapping("bpmEngine")
    public ResponseEntity<?> createBpmEngine(@RequestBody BPMConnectorDTO connectorDTO){
    	BPMEngineType type = BPMEngineType.valueOf(connectorDTO.getType());
    	switch(type) {
    		case CAMUNDA_BPM_ENGINE:
    			if(connectorDTO.getConnection() == null)
    				throw new RuntimeException("Connection missing");
    			CamundaBPMEngineFactory cemf = new CamundaBPMEngineFactory();
    	        cemf.setCamundaUrl(connectorDTO.getConnection());
    	        cemf.setName(connectorDTO.getName());
    	        cemf = bpmEngineFactoryRepository.save(cemf);
    	        break;
    		case MALICIOUS_ENGINE:
    			MaliciousEngineFactory mef = new MaliciousEngineFactory();
    	        mef.setCamundaUrl(connectorDTO.getConnection());
    	        mef.setName(connectorDTO.getName());
    	        mef = bpmEngineFactoryRepository.save(mef);
    	        break;
    	    default:
    	    	throw new RuntimeException("Unknown BPM Engine Type");
    	}
    	return ResponseEntity.ok(null);
    }
    
    @PostMapping("communicationModule")
    public ResponseEntity<?> createCommunicationModule(@RequestBody CommunicationConnectorDTO connectorDTO){
    	CommunicationModuleType type = CommunicationModuleType.valueOf(connectorDTO.getType());
    	switch(type) {
    		case DISCORD:
    			if(connectorDTO.getServerId() == null)
    				throw new RuntimeException("Server ID missing");
    			if(connectorDTO.getConnection() == null)
    				throw new RuntimeException("Connection missing");
    			DiscordAdapterFactory discordAdapterFactory = new DiscordAdapterFactory();
    	        discordAdapterFactory.setName(connectorDTO.getName());
    	        discordAdapterFactory.setDiscordServiceUrl(connectorDTO.getConnection());
    	        discordAdapterFactory.setDiscordServerId(connectorDTO.getServerId());
    	        discordAdapterFactory = communicationFactoryRepository.save(discordAdapterFactory);
    	        break;
    		case MATRIX:
    			MatrixAdapterFactory matrixAdapterFactory = new MatrixAdapterFactory();
    	        matrixAdapterFactory.setName(connectorDTO.getName());
    	        matrixAdapterFactory.setMatrixServiceUrl(connectorDTO.getConnection());
    	        matrixAdapterFactory = communicationFactoryRepository.save(matrixAdapterFactory);
    	        break;
    	    default:
    	    	throw new RuntimeException("Unknown Communication Module Type");
    	}
    	return ResponseEntity.ok(null);
    }
    
    @GetMapping("communicationModules")
    public ResponseEntity<?> getCommunicationModules(){
    	List<CommunicationFactory> factories = communicationFactoryRepository.findAll();
    	GetCommunicationConnectorDTO[] connectors = new GetCommunicationConnectorDTO[factories.size()];
    	for(int i = 0; i < factories.size(); i++) {
    		CommunicationFactory factory = factories.get(i);
    		connectors[i] = new GetCommunicationConnectorDTO();
    		connectors[i].setName(factory.getName());
    		if(factory instanceof DiscordAdapterFactory) {
    			connectors[i].setType(CommunicationModuleType.DISCORD.label);
    			connectors[i].setConnection(((DiscordAdapterFactory)factory).getDiscordServiceUrl());
    			connectors[i].setServerId(((DiscordAdapterFactory)factory).getDiscordServerId());    		}
    		else if(factory instanceof MatrixAdapterFactory) {
    			connectors[i].setType(CommunicationModuleType.MATRIX.label);
    			connectors[i].setConnection(((MatrixAdapterFactory)factory).getMatrixServiceUrl());
    		}
    		else throw new RuntimeException("Unknown communication factory type");
    		connectors[i].setUsed(isCommunicationModuleUsed(factory.getName()));
    	}
    	return ResponseEntity.ok(connectors);
    }
    
    private boolean isCommunicationModuleUsed(String name){
    	List<Alliance> alliances = allianceRepository.findAll();
    	for(Alliance alliance : alliances)
    		if(alliance.getCommunication() != null && alliance.getCommunication().getName().equals(name))
    			return true;
    	return false;
    }
    
    @GetMapping("bpmEngines")
    public ResponseEntity<?> getBpmEngines(){
    	List<BPMEngineFactory> factories= bpmEngineFactoryRepository.findAll();
    	GetConnectorDTO[] connectors = new GetConnectorDTO[factories.size()];
    	for(int i = 0; i < factories.size(); i++) {
    		BPMEngineFactory factory = factories.get(i);
    		connectors[i] = new GetConnectorDTO();
    		connectors[i].setName(factory.getName());
    		if(factory instanceof CamundaBPMEngineFactory) {
    			connectors[i].setType(BPMEngineType.CAMUNDA_BPM_ENGINE.label);
    			connectors[i].setConnection(((CamundaBPMEngineFactory)factory).getCamundaUrl());
    		}
    		else if(factory instanceof MaliciousEngineFactory)
    			connectors[i].setType(BPMEngineType.MALICIOUS_ENGINE.label);
    		else throw new RuntimeException("Unknown bpm engine factory type");
    		connectors[i].setUsed(isBpmEngineUsed(factory.getName()));
    	}
    	return ResponseEntity.ok(connectors);
    }
    
    private boolean isBpmEngineUsed(String name){
    	List<Alliance> alliances = allianceRepository.findAll();
    	for(Alliance alliance : alliances) {
    		if(alliance.getEngine().getName().equals(name))
    			return true;
    	}
    	return false;
    }
    
    @GetMapping("collaborations")
    public ResponseEntity<?> getCollaborations(){
    	List<CollaborationFactory> factories = collaborationFactoryRepository.findAll();
    	GetConnectorDTO[] connectors = new GetConnectorDTO[factories.size()];
    	for(int i = 0; i < factories.size(); i++) {
    		CollaborationFactory factory = factories.get(i);
    		connectors[i] = new GetConnectorDTO();
    		connectors[i].setName(factory.getName());
    		connectors[i].setConnection(factory.getConnection());
    		if(factory instanceof EthereumRawFactory)
    			connectors[i].setType(CollaborationType.ETHEREUM_RAW.label);
    		else if(factory instanceof HyperledgerFactory)
    			connectors[i].setType(CollaborationType.HYPERLEDGER.label);
    		else throw new RuntimeException("Unknown collaboration factory type");
    		connectors[i].setUsed(isCollaborationUsed(factory.getName()));
    	}
    	return ResponseEntity.ok(connectors);
    }
    
    public boolean isCollaborationUsed(String name){
    	List<Alliance> alliances = allianceRepository.findAll();
    	for(Alliance alliance : alliances)
    		if(alliance.getCollaboration().getName().equals(name))
    			return true;
    	return false;
    }
    
    @GetMapping("models")
    public ResponseEntity<?> getModels(){
    	List<ProcessModelFactory> factories = baseModelFactoryRepository.findAll();
    	ModelNameDTO[] nameDtos = new ModelNameDTO[factories.size()];
    	for(int i = 0; i < factories.size(); i++) {
    		String name = factories.get(i).getName();
    		nameDtos[i] = new ModelNameDTO(name, isProcessModelUsed(name));
    	}
    	return ResponseEntity.ok(nameDtos);
    }
    
    public boolean isProcessModelUsed(String modelReference){
    	List<Alliance> alliances = allianceRepository.findAll();
    	for(Alliance alliance : alliances) {
    		if(alliance.getModel().getModelReference().equals(modelReference))
    			return true;
    	}
    	return false;
    }
    
    @DeleteMapping("communicationModule")
    public ResponseEntity<?> deleteCommunicationModule(@RequestParam String name){
    	communicationFactoryRepository.deleteById(name);
    	return ResponseEntity.ok(null);
    }
    
    @DeleteMapping("collaboration")
    public ResponseEntity<?> deleteCollaboration(@RequestParam String name){
    	collaborationFactoryRepository.deleteById(name);
    	return ResponseEntity.ok(null);
    }
    
    @DeleteMapping("bpmEngine")
    public ResponseEntity<?> deleteBpmEngine(@RequestParam String name){
    	bpmEngineFactoryRepository.deleteById(name);
    	return ResponseEntity.ok(null);
    }
    
    @DeleteMapping("model")
    public ResponseEntity<?> deleteProcessModel(@RequestParam String modelReference){
    	baseModelFactoryRepository.deleteById(modelReference);
    	return ResponseEntity.ok(null);
    }

    @GetMapping("worklist")
    public ResponseEntity<?> getWorklist(@RequestParam String gir) {

        Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow(() -> new RuntimeException());
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(gir)).findFirst().orElseThrow(() -> new RuntimeException());
        List<Task> tasks = dpexService.getWorkList(alliance, instance);

        return ResponseEntity.ok(tasks);
    }
    
    @GetMapping("individualWorklist")
    public ResponseEntity<?> getIndividualWorklist(@RequestParam String gir, @RequestParam String bpmId){
        Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow(() -> new RuntimeException());
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(gir)).findFirst().orElseThrow(() -> new RuntimeException());
        List<Task> tasks = dpexService.getIndividualWorklist(alliance, instance, bpmId);

        return ResponseEntity.ok(tasks);
    }
    
    //only bpmn xml for a given gar
    @GetMapping("bpmnModel")
    public ResponseEntity<?> getBpmnModel(@RequestParam String gar){
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow();
    	CamundaMPModel model = (CamundaMPModel)alliance.getModel();
    	return ResponseEntity.ok(model.getBpmnModel());
    }
    
    //Includes the bpmn xml and the org model for a given model reference
    @GetMapping("processModel")
    public ResponseEntity<?> getProcessModel(@RequestParam String name){
    	CamundaMPModelFactory factory = (CamundaMPModelFactory)baseModelFactoryRepository.findById(name).orElseThrow();
    	GetProcessModelDTO model = new GetProcessModelDTO(new String(factory.getBpmnModel(), StandardCharsets.UTF_8), factory.getOrganizationalModel());
    	return ResponseEntity.ok(model);
    }
    
    @GetMapping("potentialTaskExecutors")
    public ResponseEntity<?> getPotentialTaskExecutors(@RequestParam String gir, @RequestParam String task){
    	Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow();
    	Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(gir)).findFirst().orElseThrow();
    	EventLog log = instance.getEventLog();
    	CamundaMPModel camundaMPModel = (CamundaMPModel)alliance.getModel();
    	BPMNModel bpmnModel = BPMNModel.fromString(new String(camundaMPModel.getBpmnModel()));
    	String orgModel = camundaMPModel.getOrganizationalModel();
    	String[] potentialTaskExecutors = OrgUtils.getPotentialTaskExecutors(task, log, orgModel, bpmnModel);
    	return ResponseEntity.ok(potentialTaskExecutors);
    }

    @PostMapping("claim")
    public ResponseEntity<?> claimTask(@RequestBody TaskActionDTO taskActionDTO) {

        Alliance alliance = allianceRepository.findByGlobalAllianceReference(taskActionDTO.getGar()).orElseThrow(() -> new RuntimeException());
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(taskActionDTO.getGir())).findFirst().orElseThrow(() -> new RuntimeException());
        User user = OrgUtils.getUserFromBpmId(alliance.getModel(), taskActionDTO.getBpmId());
        dpexService.sendTaskAction(
                alliance,
                instance,
                user,
                Task.builder().activity(taskActionDTO.getTask()).build(),
				new ProcessVariables(taskActionDTO.getProcessVariables()),
                TaskLifeCycleStage.valueOf(taskActionDTO.getTaskLifeCycleStage()));

        return ResponseEntity.ok(null);
    }


    @PostMapping("complete")
    public ResponseEntity<?> completeTask(@RequestBody TaskActionDTO taskActionDTO) {

        Alliance alliance = allianceRepository.findByGlobalAllianceReference(taskActionDTO.getGar()).orElseThrow(() -> new RuntimeException());
        Instance instance = alliance.getEngine().getInstances().stream().filter(i -> i.getGlobalInstanceReference().equals(taskActionDTO.getGir())).findFirst().orElseThrow(() -> new RuntimeException());
        User user = OrgUtils.getUserFromBpmId(alliance.getModel(), taskActionDTO.getBpmId());
        dpexService.sendTaskAction(
                alliance,
                instance,
                user,
                Task.builder().activity(taskActionDTO.getTask()).build(),
				new ProcessVariables(taskActionDTO.getProcessVariables()),
                TaskLifeCycleStage.COMPLETE);

        return ResponseEntity.ok(null);
    }


	/**
	 * This REST point is used for the upload of script files
	 *
	 * @param scriptFile
	 * @param gar
	 * @return
	 */
	@PostMapping(value="{gar}/script")
	public ResponseEntity<?> uploadScript(@RequestParam("scriptFile")MultipartFile scriptFile, @PathVariable("gar") String gar){
		List<Alliance> alliances = allianceRepository.findAll();
		Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).get();
		try{
			ScriptManager.saveFile(scriptFile, alliance.getModel().getModelReference());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return ResponseEntity.ok().build();
	}

	/**
	 * This is supposed to be called when using the camunda plugins
	 * Note: The endpoint expectes the instances localInstanceReference. The reason for that is, that the
	 * localInstanceReference is the only reference known by Camunda and therefore the Camunda frontend can
	 * only identify the instances by that
	 * @param taskActionDTO
	 * @return
	 */
	@PostMapping("camundaClaim")
	public ResponseEntity<?> camundaClaimTask(@RequestBody CamundaTaskActionDTO taskActionDTO) {
		Alliance alliance = null;
		Instance instance = null;
		List<Alliance> alliances = allianceRepository.findAll();
		for(Alliance a: alliances){
			for(Instance i: a.getEngine().getInstances()){
				if(i.getLocalInstanceReference().equals(taskActionDTO.getLir())) {
					alliance = a;
					instance = i;
				}
			}
		}
		if(alliance == null || instance == null) {
			return ResponseEntity.notFound().build();
		}
		//Alliance alliance = allianceRepository.findByLocalInstanceReference(taskActionDTO.getLir()).orElseThrow(() -> new RuntimeException());
		User user = OrgUtils.getUserFromBpmId(alliance.getModel(), taskActionDTO.getBpmId());
		dpexService.sendTaskAction(
				alliance,
				instance,
				user,
				Task.builder().activity(taskActionDTO.getTask()).build(),
				new ProcessVariables(taskActionDTO.getProcessVariables()),
				TaskLifeCycleStage.valueOf(taskActionDTO.getTaskLifeCycleStage()));

		return ResponseEntity.ok(null);
	}

	/**
	 * This is supposed to be called when using the camunda plugins. The difference to the ApiControllers complete method,
	 * is the usage of the localInstanceReference. In the ApiController, the first instance matching the globalAllianceReference is used.
	 * @param taskActionDTO
	 * @return
	 */
	@PostMapping("camundaComplete")
	public ResponseEntity<?> camundaCompleteTask(@RequestBody CamundaTaskActionDTO taskActionDTO) {
		Alliance alliance = null;
		Instance instance = null;
		List<Alliance> alliances = allianceRepository.findAll();
		for(Alliance a: alliances){
			for(Instance i: a.getEngine().getInstances()){
				if(i.getLocalInstanceReference().equals(taskActionDTO.getLir())) {
					alliance = a;
					instance = i;
				}
			}
		}
		if(alliance == null || instance == null) {
			return ResponseEntity.notFound().build();
		}
		User user = OrgUtils.getUserFromBpmId(alliance.getModel(), taskActionDTO.getBpmId());
		dpexService.sendTaskAction(
				alliance,
				instance,
				user,
				Task.builder().activity(taskActionDTO.getTask()).build(),
				new ProcessVariables(taskActionDTO.getProcessVariables()),
				TaskLifeCycleStage.COMPLETE);

		return ResponseEntity.ok(null);
	}

	@GetMapping(value="allAlliances")
	public ResponseEntity<?> getAlliances() {
		return ResponseEntity.ok().body(allianceRepository.findAll());
	}
}

@Data
@AllArgsConstructor
class ModelNameDTO{
	private String name;
	private boolean used;
}

@Data
@AllArgsConstructor
class GetProcessModelDTO{
	private String bpmnModelXml;
	private String orgModel;
}

@Data
class BPMConnectorDTO{
	@JsonProperty(required = true)
	private String type;
	
	@JsonProperty(required = false)
	private String connection;
	
	@JsonProperty(required = true)
	private String name;
}

@Data
class CollaborationConnectorDTO{
	@JsonProperty(required = true)
	private String type;
	
	@JsonProperty(required = true)
	private String connection;
	
	@JsonProperty(required = true)
	private String name;
	
	@JsonProperty(required = false)
	private String apiKey = " ";
}

@Data
class CommunicationConnectorDTO{
	@JsonProperty(required = true)
	private String type;
	
	@JsonProperty(required = false)
	private String connection;
	
	@JsonProperty(required = true)
	private String name;
	
	@JsonProperty(required = false)
	private String serverId;
}

@Data
class GetConnectorDTO{
	private String type;
	private String connection;
	private String name;
	private boolean used;
}

@Data
@EqualsAndHashCode(callSuper=true)
class GetCommunicationConnectorDTO extends GetConnectorDTO{
	private String serverId;
}

@Data
@AllArgsConstructor
class EnumValueDTO {
	private String value;
	private String label;
}

@Data
@AllArgsConstructor
class GetAllianceDTO{
	private String name;
	private String gar;
	private String processModel;
	private ParticipantDTO[] participants;
	private Instance[] instances;
	private String communicationModule;
	private String collaboration;
	private String bpmEngine;
}

@Data
class DeploySciDTO{
	@JsonProperty(required = true)
	private String sciId;
	
	@JsonProperty(required = true)
	private String connection;
	
	@JsonProperty(required = false)
	private String apiKey = " ";
}

@Data
class ProcessModelDTO {

    private MultipartFile processModel;
    private String organizationalModel;
}

@Data
@AllArgsConstructor
class AllianceDTO {

    private String name;
    private String GAR;

    private String processModel;

    private String bpmEngine;
    private String collaboration;

    private String communication;


}

@Data
class InstantiateDTO {
    private String gar;
    private String bpmId;
    private String name;
    private String variables;
}

@Data
class TaskActionDTO {
    private String gar;
    private String gir;
    private String task;
    private String taskLifeCycleStage;
    private String processVariables;
    private String bpmId;

}

@Data
class CamundaTaskActionDTO {
	private String lir;
	private String task;
	private String taskLifeCycleStage;
	private String processVariables;
	private String bpmId;
}