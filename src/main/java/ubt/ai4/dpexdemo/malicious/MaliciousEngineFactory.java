package ubt.ai4.dpexdemo.malicious;

import dpex.bpm.execution.BPMEngine;
import dpex.bpm.execution.BPMEngineFactory;
import dpex.bpm.model.ProcessModel;
import dpex.impl.bpm.camunda.CamundaExecutionEngine;
import dpex.impl.bpm.camunda.CamundaExecutionEngineFactory;
import dpex.impl.bpm.camunda.CamundaModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MaliciousEngineFactory extends CamundaExecutionEngineFactory {

    private String camundaUrl;

	@Override
    public MaliciousEngine create(ProcessModel processModel) {
        MaliciousEngine me = new MaliciousEngine();
		
		me.deploy(((CamundaModel) processModel).getBpmnModel());
        
		me.setCamundaUrl(this.camundaUrl);
        me.setName(super.getName());
        return me;
    }
}
