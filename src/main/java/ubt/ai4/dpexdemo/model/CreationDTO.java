package ubt.ai4.dpexdemo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreationDTO {
    private String connection;
    private String type;

    public CreationDTO() {

    }

    public CreationDTO(String connection, String type) {
        this.connection = connection;
        this.type = type;
    }
}
