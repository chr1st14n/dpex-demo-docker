package ubt.ai4.dpexdemo.model;

public class AvailableAdapterDTO {
    private String name;
    private String connection;

    public AvailableAdapterDTO(String name, String connection) {
        this.name = name;
        this.connection = connection;
    }

    public AvailableAdapterDTO(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }
}
