package ubt.ai4.dpexdemo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteDTO {
    //When deleting an alliance the name represents the GAR
    private String name;
    private String kind;


    public DeleteDTO() {
    }
}
