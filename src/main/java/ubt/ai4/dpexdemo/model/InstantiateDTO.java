package ubt.ai4.dpexdemo.model;

import lombok.Data;

@Data
public
class InstantiateDTO {
    private String gar;
    private String publicKey;
    private String privateKey;
    private String variables;
}
