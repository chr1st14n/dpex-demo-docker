package ubt.ai4.dpexdemo.model;

import lombok.Data;

@Data
public
class TaskActionDTO {
    private String gar;
    private String gir;
    private String task;
    private String taskLifeCycleStage;
    private String processVariables;
    private String userReference;
    private String publicKey;
    private String privateKey;
}
