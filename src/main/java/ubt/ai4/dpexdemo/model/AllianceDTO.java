package ubt.ai4.dpexdemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public
class AllianceDTO {

    private String name;
    private String GAR;

    private String processModel;

    private String bpmEngine;
    private String collaboration;

}
