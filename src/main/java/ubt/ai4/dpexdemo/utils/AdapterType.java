package ubt.ai4.dpexdemo.utils;

public enum AdapterType {
    BPM("BPM"),
    COMMUNICATION("communication"),
    COLLABORATION("collaboration"),
    ALLIANCE("alliance");
    public final String label;

    private AdapterType(String label) {
        this.label = label;
    }

    public static AdapterType getMatchingAdapterType(String label){
        switch (label){
            case "BPM":
                return BPM;
            case "communication":
                return COMMUNICATION;
            case "collaboration":
                return COLLABORATION;
            case "alliance":
                return ALLIANCE;
            default:
                return null;
        }
    }
}
