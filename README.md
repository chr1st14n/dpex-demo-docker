
  

Disclamer: The purpose of the project in its current state is to demonstrate the feasibility of the approach presented in the paper.

Additional information of the paper will be included in the README file later.

  
# Screencast
**A video of the showcase demonstration can be found [YouTube](https://www.youtube.com/watch?v=r8rWPDHLOzQ)**.

  

# dpex-demo: A showcase for dpex-lib

  

This repository contains the **dpex-demo** project. **dpex-demo** is a runnable Spring Boot application that integrates the **dpex-lib** library for **d**ecentralized **p**rocess **ex**ecution. The **dpex-demo** application demonstrates the usage of the **dpex-lib** approach and was used to implement the business trip application use case at the University of Bayreuth.

  

The repository of the **dpex-lib** project can also be found on GitLab [**here**](https://gitlab.com/bpm-dpex/dpex-lib).

  

# Getting Started

  

This tutorial guides through the steps to reproduce the business trip application use case.

Thereby, the **Camunda WFMS** (integrated in the application) will provide process execution functionalities, and the **Ethereum protocol** (simulated with Ganache) addresses the requirements of a **secured communication infrastructure (SCI)**.

  

## Build the project

  

This section guides through the manual build process. Insted, you can also **download the packaged Spring Boot application** [here](https://gitlab.com/bpm-dpex/dpex-demo/-/pipelines).

  

### Requirements

  

- Java 17

- npm 6.14.10 or [Ganache](https://github.com/trufflesuite/ganache-ui/releases/tag/v2.7.0) (Ganache-2.7.0-win-x64-setup.exe)

  

### Build the Project

- Clone the repository to your local system

> ```git clone https://gitlab.com/bpm-dpex/dpex-demo```

- Navigate into the dpex-lib folder and build the project with

> ```./gradlew build```

- Run the Spring Boot application with

> ```java -jar ./build/libs/dpex-demo-0.0.1-SNAPSHOT.jar```

  

## Start your local Ethereum Blockchain test net

To streamline the reproduction of results, we rely on a simulation of the Ethereum network, instead of setting up a *real* network. Therefore, you can choose from the **ganache cli** (recommended) or the **Ganache Application**.

#### ganache cli

- Open a new Terminal window and install the [ganache](https://github.com/trufflesuite/ganache) command line interface

> ```npm install ganache --global```

- Start the blockchain simulation (*in the new Terminal window*) with the following command. Use an appropriate block gas limit (```-l 60000000```), a moderate time span for new blocks (```-b 10```) and available accounts that fits our use case (```-m "..."```)

> ```ganache -l 60000000 -b 10 -m "shiver armed industry victory sight vague follow spray couple hat obscure yard"```

#### Ganache Application (untested)

- Download and Install the [Ganache](https://github.com/trufflesuite/ganache-ui/releases/tag/v2.7.0)

- Configure your local blockchain as described above.

  
  

## Replay the test use case

  

### Requirements

- Postman 10 (optional, for sending HTTP Requests to dpex-demo)

  

### Use Case

  

#### Process Model

![Headlight production](./src/main/resources/Scheinwerferproduktion.png)

#### Organizational Model

The organizational model is described within a CSV file.

The first entry is the BPM ID, followed by the SCI ID (e.g. Ethereum wallet address) and Communication-ID (e.g. Matrix ID). Then a potential homeserver URL can be specified. This property may be omitted for communication protocols that don't support homeservers or don't need any additional information. If no communication module is used at all, communication ID and homeserver URL may both be empty. Then the organizational model defines the roles assigned to that actor as a list, devided by '|'. Finally, the departments of the actor are given as a list aswell.

```
BpmId,SciId,CommunicationId,HomeserverUrl,Roles,Departments
GE,0xE076df4e49182f0AB6f4B98219F721Cccc38f9be,@ge_:matrix.org,https://matrix.org,Einkauf,Scheinwerferhersteller
OX,0x4B1184629DE85ab53cF86477D190a9f3740ABdF5,@ox_:matrix.org,https://matrix.org,Produktion,Scheinwerferhersteller
GU,0x33d329263B56742607E2710a7CC5D927F1F279d3,@g.u_:matrix.org,https://matrix.org,Produktion,Scheinwerferhersteller
VS,0x862C252D5e3fd90436CfC17d41169a0B85872471,@v.s_:matrix.org,https://matrix.org,Qualitätsmanagement,Scheinwerferhersteller
BM,0x422039b408CeCb32361F9D5EF3E2Eb5c0b0d0Cc2,@b.m_:matrix.org,https://matrix.org,Produktion,LED-Hersteller
NV,0x21aF08CDAC443f2f8de351c515ABbe3bB5DB9b90,@nv_:matrix.org,https://matrix.org,Produktion,LED-Hersteller
CN,0x7aFCd0B490bc1a5b142EBC455ef879F872ea8AFa,@c.n_:matrix.org,https://matrix.org,Qualitätsmanagement,LED-Hersteller
IU,0xbc1535f4D9Ee27b79964fDC47E986DcfA4d8A64c,@iu_:matrix.org,https://matrix.org,Vertrieb,LED-Hersteller
```

For example, the given organizational model specifies that ```GE``` has the role ```Einkauf``` and works for the department ```Scheinwerferhersteller```. In addition he/she can be contacted via @ge_:matrix.org which is hosted on https://matrix.org.

The organizational model must be agreed upon and distributed over all participants along with the process model.

### Execute the Process

Make sure that both are up and running, the Spring Boot application and the blockchain network. Import the **dpex-demo Workspace** in Postman. The workspace can be found [here](https://www.postman.com/csai4/workspace/dpex-demo/collection/21953992-71722291-dc88-4059-b748-a814a27516b9). Please consider this workspace for the specification of the request bodies. Consider exporting the collection and importing it in your local Postman instance.

  

#### Setup

1. Deploy the SCI contract

> ```POST http://.../api/ethereum/deploySCI```

Request Body (JSON):

```
{
    "sciId": "0x...",
    "connection": "localhost:8545"
}
```

This first step consists of deploying a smart contract that will manage the creation of all instances of an alliance in order to notify the participants that have registered an event listener. Therefore the returned contract address has to be used as Global Alliance Reference (GAR) when creating the alliance. For the SCI ID an arbitrary Ganache wallet adress can be chosen

2. Create a Process Model (with Organizational Model)

> ```POST http://.../api/model```

Request Body (Form-Data):

```
processModel=[file]
organizationalModel="BpmId,SciId,CommunicationId,HomeserverUrl,Roles,Departments\nGE..."
```

Secondly, a process model must be uploaded to the application for later usage. Please note that the ```LINE_SPLITTER``` in the ```OrgUtils``` class has to be changed to ```\n``` when using postman. By default it is ```\r\n``` in order to work with the dpex-Frontend.

3. Create Collaboration Connector

> ```POST http://.../api/collaboration```

Request Body (JSON):

```
{
    "type": "ETHEREUM_RAW",
    "connection": "http://localhost:8545",
    "name": "Ethereum"
}
```

The type must be one of ```ETHEREUM_RAW``` or ```HYPERLEDGER```. More connectors may be implemented in the future. The connection is the URL where the SCI is running and the name is arbitrary.

4. Create BPM Connector

> ```http://.../api/bpmEngine```

Request Body (JSON):

```
{
    "connection": "http://localhost:8080/engine-rest",
    "type": "CAMUNDA_BPM_ENGINE",
    "name": "Camunda"
}
```

Currently only the type CAMUNDA_BPM_ENGINE is supported.

5. Create Communication Connector (optional)

> ```http://.../api/communicationModule```

Request Body (JSON):

```
{
    "type": "MATRIX",
    "connection": "http://localhost:3001",
    "name": "Matrix"
}
```

If you don't need integrated messaging functionality in DPEX, this step can be omitted. Two types for communication connectors currently exist: ```MATRIX``` and ```DISCORD```. The connection attribute has to contain the URL on which the respective ExpressJS-Microservice is running. And the name has to equal the name that you give to the microservice as second program argument when launching it.

6. Create an Alliance Object

> ```POST http://.../api/alliance```

Request Body (Form-Data):

```
name="Headlight Production",
GAR="0x...",
processModel="Scheinwerferproduktion",
bpmEngine="Camunda Integrated Engine",
collaboration="Ethereum Raw",
[communication="Matrix"]
```

Please validate that the ```GAR``` is the adress of the smart contract that has been received in step 1 and that the ```processModel``` conforms to the ```id``` attribute of the ```<bpmn:process>``` element in the uploaded file of step 2. Furthermore the values of ```bpmEngine```, ```collaboration``` and ```communication``` have to equal the names of the corresponding connectors of the steps 3-5, whilst the communication attribute may be omitted if not needed.

7. Instantiate the Process Model

> ```POST http://.../api/instantiate```

Request Body (JSON):

```
{
    "gar": "0x...",
    "bpmId": "GE",
    "name": "My Instance",
    "variables": ""
}
```

This will trigger a blockchain transaction. We must wait for its validation, before we can retrieve any results in step 8. Please validate that the ```gar``` refers to the ```GAR``` value specified in step 6. The ```bpmId``` is used to retreive the associated Ethereum wallet with which the transaction will be sent. In the ```variables``` field, process variables can be specified as a JSON formatted string.

During this step, a smart contract is deployed on the blockchain. Its contract address will be used as Global Instance Reference (GIR). The only functionality of the smart contract is as follows. To claim or complete a task, a participant invokes a simple smart contract function and provides relevant information. When this smart contract invocation is recorded as transaction in the blockchain, the smart contract emits this relvant information as event. All participant register an event listener to this specific smart contract, to get notified on new events.

#### Execution

8. Retrieve the available instances

> ```GET http://.../api/instance?gar=0x...```

Response Body:

```
[
    {
        "localInstanceReference": "4a8e2992...",
        "globalInstanceReference": "0x...",
        "eventLog": {
            "id": 1,
            "events": []
        }
    }
]
```

Here, we will fetch a list of all available instances of a given alliance. Hence, please validate that the ```gar```-Request Parameter refers to the global alliance reference specified (```GAR```) in step 6. The ```globalInstanceReference``` (```gir```) in the Response Body refers to the contract address that was deployed in step 7.

9. Retrieve the work list

> ```GET http://.../api/worklist?gar=0x...&globalInstanceReference=0x...```

Response Body:

```
[
    {
        "activity": "Bestellung LED",
        "resource": null
    },
    {
        "activity": "Produktion LED",
        "resource": null
    }
]
```

We can review the current open work items with respect to a ```GAR``` (global alliance reference) and a ```GIR``` (global instance reference). Currently, no ```resource``` is assigned. This can be achieved in the next step.

10. Claim Task

> ```POST http://.../api/claim```

  

Request Body (JSON):

```
{
    "gar": "0x...",
    "gir": "0x...",
    "task": "Bestellung LED",
    "taskLifeCycleStage": "CLAIM",
    "bpmId": "GE",
    "processVariables": ""
}
```

To claim a task, we must specify the following information in the Request Body. The ```gar``` and ```gir```, the ```task``` we refer to along with the respective ```lifeCycleStage``` which is ```CLAIM``` when we want to claim a task and the ```bpmId``` of the user that claims the task. Lastly, ```processVariables``` can be specified as a JSON formatted string.

11. Complete Task

> ```POST http://.../api/complete```

Request Body (JSON):

```
{
    "gar": "0x...",
    "gir": "0x...",
    "task": "Bestellung LED",
    "taskLifeCycleStage": "COMPLETE",
    "bpmId": "GE",
    "processVariables": ""
}
```

The Request Body when we want to complete a task is similar to step 10. However, we have to use the ```taskLifeCycleStage``` ```COMPLETE``` here.

12. Complete Task with Process Variables

> ```POST http://.../api/complete```

```
{
    "gar": "0x...",
    "gir": "0x...",
    "task": "Bestellung LED",
    "taskLifeCycleStage": "COMPLETE",
    "bpmId": "GE",
    "processVariables": "{\"productLedOk\": true}"
}
```

Let's take the example of the task ```Qualitätskontrolle LED```. Here the executing actor has to decide whether the LED passes the quality check or not which will determine the outgoing branch of the following XOR gateway. Therefore a process variable ```productLedOk``` is defined to specify that the LED has passed the quality check. The condition expressions of the outgoing flows of the XOR gateway must then be implemented using that variable.

#### Fraud Prevention

ToDo

# Structure of the Project

  

The most important packages of the **dpex-lib** project include ```ubt.ai4.dpexdemo.api```, ```ubt.ai4.dpexdemo.camunda.*```, ```ubt.ai4.dpexdemo.org.*```. Their core classes and their role in dpex are described next.

  

### ```ubt.ai4.dpexdemo.api```

t.b.d.

### ```ubt.ai4.dpexdemo.camunda.*```

t.b.d.

### ```ubt.ai4.dpexdemo.org.*```

t.b.d.

# The communication API

## Creating a connector

In order to be able to work with communication modules you need to create a connector first. A connector specifies the type (currently "MATRIX" and "DISCORD" are supported), the connection on which the corresponding microservice is running (if the communication module runs without a microservice this field can be omitted) and the name of the connector. In the case of Matrix and Discord the name has to be identical to the name you specify when starting the microservice.

> ```POST http://.../api/communicationModule```

Body:
```
{
    "type": "MATRIX",
    "connection": "http://localhost:3001",
    "name": "Matrix"
}
```

This example request will create a Matrix-Connector to a Microservice which runs of localhost:3001 and is called "Matrix". You would start the microservice using the following command:

```npm start 3001 Matrix```

For more details see https://gitlab.com/bpm-dpex/dpex-communication-matrix-adapter and https://gitlab.com/bpm-dpex/dpex-communication-discord-adapter

## API

After the setup of the connector is done, it is possible to use the communication API. Some of the most important requests are:

### Login

Login in to your matrix account using your credentials and retrieve an accessToken.

> ```POST http://.../api/communication/login```

Body:
```
{
    "userId": "@user.id:matrix.org",
    "password": "xxxxxxxxxxxxxx",
    "communicationModule": "Matrix"
}
```

### Create space

Create the space for the alliance. The space will automatically be called the same as the alliance and all of its participants will be invited.

> ```POST http://.../api/communication/createSpace```

Authorization-Header: [accessToken]

Body:
```
{
    "gar": "0x3750be6d627b600d498150cc847a49e3f677c6c7"
}
```

### Create room by org-query

Create a room by specifying its members by organizational constraints. For example: "Create a room with all research assistants that belong to the department AI4."

> ```POST http://.../api/communication/createRoom```

Authorization-Header: [accessToken]

Body:
```
{
    "spaceId": "!aBcDeFgHiJkLmNoP:matrix.org",
    "name": "Research Assistants AI4",
    "gar": "0x3750be6d627b600d498150cc847a49e3f677c6c7",
    "orgQuery": {
        "department": "AI4",
        "role": "Research Assistant"
    }
}
```

### Send message to task executor

Send a message to the resource that has executed a specified task.
Note: Since the *global* instance reference is unique across all alliances and instances, the gar can be omitted.

> ```POST http://.../api/communication/sendMessageToTaskExecutor```

Authorization-Header: [accessToken]

Body:
```
{
    "gir": "0x30b4a157e0a3fa8a81a63da5a710e82871c5f177",
    "task": "Apply for trip",
    "message": "Lorem ipsum"
}
```